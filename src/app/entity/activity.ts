export default class Activity {
    name: string;
    location: string;
    description: string;
    period : string;
    Date: string;
    Time: string;
    host: string;
}
