export default class Student {
  studentId: number;
  name: string;
  surname: string;
  date: string;
  image: string;
  email: string;
  password: string;
}
